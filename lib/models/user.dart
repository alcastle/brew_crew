//  Result: FirebaseUser({
// uid: SaluNTHF02RxQTAWXxd7zmXfs702,
// isAnonymous: true,
// providerData: [
//   {
//     uid: SaluNTHF02RxQTAWXxd7zmXfs702,
//     providerId: firebase
//   }
// ],
// providerId: firebase,
// creationTimestamp: 1601086597581,
// lastSignInTimestamp: 1601086597581,
// isEmailVerified: false
// })

class User {
  final String uid;
  final bool isEmailVerified;
  final bool isAnonymous;
  // final Timestamp createdAt;
  // final Timestamp lastSignInAt;

  User({
    this.uid,
    this.isEmailVerified,
    this.isAnonymous,
    // this.createdAt,
    // this.lastSignInAt,
  });
}

class UserData {
  final String uid;
  final String name;
  final String sugars;
  final int strength;

  UserData({this.uid, this.name, this.sugars, this.strength});
}

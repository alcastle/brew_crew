import 'package:brew_crew/models/user.dart';
import 'package:brew_crew/services/database.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:form_field_validator/form_field_validator.dart';

class AuthService {
  final FirebaseAuth _auth = FirebaseAuth.instance;

  final passwordValidator = MultiValidator([
    RequiredValidator(errorText: 'Password is required'),
    MinLengthValidator(8, errorText: 'Must be at least 8 digits long'),
    PatternValidator(r'(?=.*?[#?!@$%^&*-])',
        errorText: 'Include a special character')
  ]);

  matchValidator(String pass1, String pass2) {
    if (pass1.length == 0 || pass1 == null) {
      pass1 = 'monkeyspasm';
    }
    return MatchValidator(errorText: 'Passwords do not match')
        .validateMatch(pass1, pass2);
  }

  final emailValidator = MultiValidator([
    RequiredValidator(errorText: 'Email is required'),
    EmailValidator(errorText: 'Invalid Email Address')
  ]);

  final requiredValidator = RequiredValidator(errorText: 'Required');
  String errorMessage;

  // create user object based on FirebaseUser
  User _user(FirebaseUser user) {
    print(user.toString());

    if (user != null) {
      return User(
        uid: user.uid,
        isAnonymous: user.isAnonymous,
        isEmailVerified: user.isEmailVerified,
      );
    } else {
      return null;
    }
  }

  // Stream watch for auth changes
  // Turn a FirebaseUser into our User object
  Stream<User> get user {
    // This first return is the long way of saying the latter line
    //return _auth.onAuthStateChanged.map((FirebaseUser user) => _user(user));
    return _auth.onAuthStateChanged.map(_user);
  }

  // sign in anon
  Future signInAnon() async {
    try {
      AuthResult result = await _auth.signInAnonymously();
      FirebaseUser user = result.user;
      return _user(user);
    } catch (e) {
      print(e.toString());
    }
  }

  // sign in with email / pass
  Future signIn(String email, String password) async {
    try {
      AuthResult result = await _auth.signInWithEmailAndPassword(
          email: email, password: password);
      FirebaseUser user = result.user;

      return _user(user);
    } catch (e) {
      print('Code: ' + e.code);
      print('Mesg: ' + e.message);
      switch (e.code) {
        case "ERROR_INVALID_EMAIL":
          errorMessage = "Your email address appears to be malformed.";
          break;
        case "ERROR_WRONG_PASSWORD":
          errorMessage = "Your password is wrong.";
          break;
        case "ERROR_USER_NOT_FOUND":
          errorMessage = "User with this email doesn't exist.";
          break;
        case "ERROR_USER_DISABLED":
          errorMessage = "User with this email has been disabled.";
          break;
        case "ERROR_TOO_MANY_REQUESTS":
          errorMessage = "Too many requests. Try again later.";
          break;
        case "ERROR_OPERATION_NOT_ALLOWED":
          errorMessage = "Signing in with Email and Password is not enabled.";
          break;
        default:
          errorMessage = e.message;
      }
      return errorMessage;
    }
  }

  // register with email / pass
  // Firebase has crap error handling
  // https://gist.github.com/nikhilmufc7/6b74a3c12a6e2d3284942d40ff583e37
  // https://github.com/flutter/flutter/issues/20223#issue-347613208
  Future register(String email, String password) async {
    try {
      AuthResult result = await _auth.createUserWithEmailAndPassword(
          email: email, password: password);
      FirebaseUser user = result.user;

      // Create a new document for this user
      await DatabaseService(uid: user.uid).updateUserData('0', 'new', 100);

      return _user(user);
    } catch (e) {
      print('Code: ' + e.code);
      print('Auth Error:' + e.message);
      switch (e.code) {
        case "ERROR_OPERATION_NOT_ALLOWED":
          errorMessage = 'Anonymous accounts are not allowed';
          break;
        case "ERROR_WEAK_PASSWORD":
          errorMessage = "The password is too weak";
          break;
        case "ERROR_INVALID_EMAIL":
          errorMessage = "Your email address appears to be malformed";
          break;
        case "ERROR_EMAIL_ALREADY_IN_USE":
          errorMessage = "Email is already in use";
          break;
        case "ERROR_INVALID_CREDENTIAL":
          errorMessage = "Something happened";
          break;
        default:
          errorMessage = e.message;
      }

      return errorMessage;
    }
  }

  // sign out
  Future signOut() async {
    try {
      return await _auth.signOut();
    } catch (e) {
      print(e.toString());
    }
  }
}

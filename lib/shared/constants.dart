import 'package:flutter/material.dart';

const textInputDecoration = InputDecoration(
  fillColor: Colors.white,
  filled: true,
  enabledBorder: OutlineInputBorder(
    borderSide: BorderSide(color: Colors.grey, width: 2),
  ),
  focusedBorder: OutlineInputBorder(
    borderSide: BorderSide(color: Colors.pinkAccent, width: 2),
  ),
  //border: InputBorder.none,
  //labelText: 'Enter your email',
  labelStyle:
      TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 22),
  //hintText: 'Email Address',
  hintStyle: TextStyle(color: Colors.red, fontSize: 16),
  //icon: Icon(Icons.email, color: Colors.white)
);

const emailIcon = Icon(Icons.email, color: Colors.white);

const passwordIcon = Icon(Icons.lock, color: Colors.white);

import 'package:brew_crew/models/user.dart';
import 'package:brew_crew/services/database.dart';
import 'package:brew_crew/shared/loading.dart';
import 'package:flutter/material.dart';
import 'package:brew_crew/shared/constants.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:provider/provider.dart';

class SettingsForm extends StatefulWidget {
  @override
  _SettingsFormState createState() => _SettingsFormState();
}

class _SettingsFormState extends State<SettingsForm> {
  final _formKey = GlobalKey<FormState>();
  final List<String> sugars = ['0', '1', '2', '3', '4', '5'];

  // Form values
  String _currentSugars;
  String _currentName;
  int _currentStrength;

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User>(context);

    return StreamBuilder<UserData>(
        stream: DatabaseService(uid: user.uid).userData,
        builder: (context, snapshot) {
          // check to see if we have data from the userData stream
          // Flutters implimentation of data coming down the stream
          // This doesn't have anything to do with firebase
          if (snapshot.hasData) {
            UserData userData = snapshot.data;
            return Form(
              key: _formKey,
              child: Column(
                children: <Widget>[
                  Text(
                    'Update Settings',
                    style: TextStyle(
                      fontSize: 18,
                      fontStyle: FontStyle.italic,
                      letterSpacing: 3,
                    ),
                  ),

                  SizedBox(height: 30),

                  TextFormField(
                    initialValue: userData.name,
                    decoration: textInputDecoration.copyWith(
                        labelText: 'Enter your name'),
                    validator: RequiredValidator(errorText: 'Required'),
                    onChanged: (val) => setState(() => _currentName = val),
                  ),

                  SizedBox(height: 20),

                  // Dropdown
                  DropdownButtonFormField(
                    decoration:
                        textInputDecoration.copyWith(labelText: 'Sugars'),
                    value: _currentSugars ?? userData.sugars,
                    items: sugars.map((sugar) {
                      return DropdownMenuItem(
                        value: sugar,
                        child: Text('$sugar sugars'),
                      );
                    }).toList(),
                    onChanged: (val) => setState(() => _currentSugars = val),
                  ),

                  SizedBox(height: 30),

                  // Slider
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      '  Strength',
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w800,
                        letterSpacing: 0.3,
                      ),
                    ),
                  ),
                  Slider(
                    label: 'Brew Strength ' +
                        (_currentStrength ?? userData.strength).toString(),
                    activeColor:
                        Colors.brown[_currentStrength ?? userData.strength],
                    inactiveColor: Colors.grey,
                    value: (_currentStrength ?? userData.strength).toDouble(),
                    min: 100,
                    max: 900,
                    divisions: 8,
                    onChanged: (val) =>
                        setState(() => _currentStrength = val.round()),
                  ),

                  SizedBox(height: 20),

                  // Button
                  RaisedButton(
                    color: Colors.pink[400],
                    child: Text(
                      'Update',
                      style: TextStyle(color: Colors.white),
                    ),
                    onPressed: () async {
                      if (_formKey.currentState.validate()) {
                        await DatabaseService(uid: userData.uid).updateUserData(
                            _currentSugars ?? userData.sugars,
                            _currentName ?? userData.name,
                            _currentStrength ?? userData.strength);
                        // Collapse the settings - bottom sheet
                        Navigator.pop(context);
                      }
                      print(_currentName);
                      print(_currentStrength);
                      print(_currentSugars);
                    },
                  ),
                ],
              ),
            );
          } else {
            return Loading();
          }
        });
  }
}

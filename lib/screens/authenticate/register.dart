import 'package:brew_crew/models/user.dart';
import 'package:brew_crew/services/auth.dart';
import 'package:brew_crew/shared/constants.dart';
import 'package:brew_crew/shared/loading.dart';
import 'package:flutter/material.dart';

class Register extends StatefulWidget {
  final Function toggleView;

  Register({this.toggleView});

  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  final AuthService _auth = AuthService();
  // Keeps track of the state of the form
  // See the Form widget with value "key"
  final _formKey = GlobalKey<FormState>();
  bool loading = false;

  String email = '';
  String pass1 = '';
  String pass2 = '';
  String error = '';

  @override
  Widget build(BuildContext context) {
    return loading
        ? Loading()
        : Scaffold(
            backgroundColor: Colors.black,
            appBar: AppBar(
              backgroundColor: Colors.blueGrey,
              elevation: 0.0,
              title: Text('Register'),
              actions: <Widget>[
                FlatButton.icon(
                  icon: Icon(Icons.person),
                  label: Text('LogIn'),
                  onPressed: () {
                    widget.toggleView();
                  },
                ),
              ],
            ),
            body: ListView(children: [
              Container(
                padding: EdgeInsets.symmetric(vertical: 20, horizontal: 50),
                child: Form(
                  key: _formKey,
                  autovalidate: true,
                  child: Column(
                    children: <Widget>[
                      SizedBox(height: 20),

                      // Email Form Field
                      TextFormField(
                        validator: _auth.emailValidator,
                        onChanged: (value) {
                          setState(() {
                            email = value.trim();
                          });
                        },
                        autofocus: true,
                        cursorColor: Colors.black,
                        style: TextStyle(color: Colors.black),
                        decoration: textInputDecoration.copyWith(
                            hintText: 'Email Address', icon: emailIcon),
                      ),
                      SizedBox(height: 20),

                      // Password Form Field
                      TextFormField(
                          validator: _auth.passwordValidator,
                          onChanged: (value) {
                            setState(() {
                              pass1 = value.trim();
                            });
                          },
                          obscureText: true,
                          cursorColor: Colors.black,
                          style: TextStyle(color: Colors.black),
                          decoration: textInputDecoration.copyWith(
                              hintText: 'Password', icon: passwordIcon)),

                      SizedBox(height: 20),

                      // Password 2
                      TextFormField(
                        validator: (value) =>
                            _auth.matchValidator(pass1, pass2),
                        onChanged: (value) {
                          setState(() {
                            pass2 = value.trim();
                          });
                        },
                        obscureText: true,
                        cursorColor: Colors.black,
                        style: TextStyle(color: Colors.black),
                        decoration: textInputDecoration.copyWith(
                            hintText: 'Password Again', icon: passwordIcon),
                      ),

                      SizedBox(height: 20),

                      // Action Button
                      RaisedButton(
                        color: Colors.red,
                        onPressed: () async {
                          // Check if the form is valid
                          if (_formKey.currentState.validate()) {
                            loading = true;
                            dynamic result = await _auth.register(email, pass1);
                            if (mounted) {
                              if (result != User) {
                                setState(() {
                                  error = result;
                                  loading = false;
                                });
                              }
                            }
                            print('"' + email + '"');
                            print(pass1);
                            print(pass2);
                          }
                        },
                        child: Text(
                          'Register',
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      SizedBox(height: 12),
                      Text(
                        error,
                        style: TextStyle(
                          color: Colors.red,
                          fontSize: 14.0,
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ]),
          );
  }
}

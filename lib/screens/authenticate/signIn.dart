import 'package:brew_crew/models/user.dart';
import 'package:brew_crew/services/auth.dart';
import 'package:brew_crew/shared/constants.dart';
import 'package:brew_crew/shared/loading.dart';
import 'package:flutter/material.dart';

class SignIn extends StatefulWidget {
  final Function toggleView;

  // Constructor
  SignIn({this.toggleView});

  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  final AuthService _auth = AuthService();
  final _formKey = GlobalKey<FormState>();
  bool loading = false;

  // Text field state
  String email = '';
  String pass = '';
  String error = '';

  @override
  Widget build(BuildContext context) {
    return loading
        ? Loading()
        : Scaffold(
            backgroundColor: Colors.black,
            appBar: AppBar(
              backgroundColor: Colors.blueGrey,
              elevation: 0.0,
              title: Text('Log In'),
              actions: <Widget>[
                FlatButton.icon(
                  icon: Icon(Icons.person),
                  label: Text('Register'),
                  onPressed: () {
                    widget.toggleView();
                  },
                ),
              ],
            ),
            body: ListView(children: [
              Container(
                padding: EdgeInsets.symmetric(vertical: 20, horizontal: 50),
                child: Form(
                  autovalidate: true,
                  key: _formKey,
                  child: Column(
                    children: <Widget>[
                      SizedBox(height: 20),

                      // Email Form Field
                      TextFormField(
                        validator: _auth.emailValidator,
                        onChanged: (value) {
                          setState(() {
                            email = value.trim();
                          });
                        },
                        autofocus: true,
                        cursorColor: Colors.black,
                        style: TextStyle(color: Colors.black),
                        decoration: textInputDecoration.copyWith(
                            hintText: 'Email Address', icon: emailIcon),
                      ),

                      SizedBox(height: 20),

                      // Password Form Field
                      TextFormField(
                        obscureText: true,
                        cursorColor: Colors.black,
                        style: TextStyle(color: Colors.black),
                        decoration: textInputDecoration.copyWith(
                            hintText: 'Password', icon: passwordIcon),
                        validator: _auth.requiredValidator,
                        onChanged: (value) {
                          setState(() {
                            pass = value.trim();
                          });
                        },
                      ),

                      SizedBox(height: 20),

                      RaisedButton(
                        color: Colors.red,
                        onPressed: () async {
                          // Check if the form is valid - loading screen
                          if (_formKey.currentState.validate()) {
                            setState(() => loading = true);
                            dynamic result = await _auth.signIn(email, pass);
                            if (result != User) {
                              setState(() {
                                error = result;
                                loading = false;
                              });
                            }
                            print('"' + email + '"');
                            print(pass);
                          }
                          print(email + '::' + pass);
                        },
                        child: Text(
                          'Log In',
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),

                      SizedBox(height: 12),

                      Text(
                        error,
                        style: TextStyle(
                          color: Colors.red,
                          fontSize: 14.0,
                        ),
                      ),
                    ],
                  ),
                ),
                // child: RaisedButton(
                //     child: Text('Sign In Anon'),
                //     onPressed: () async {
                //       dynamic userResult = await _auth.signInAnon();
                //       if (userResult == null) {
                //         print('Error signing in');
                //       } else {
                //         print('User signed in');
                //       }
                //       print('Result: ' + userResult.uid);
                //     }),
              ),
            ]),
          );
  }
}
